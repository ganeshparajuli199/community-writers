### What is your audience?

[The knowledge required from the reader (basic, intermediate, advanced). Example: "_sysadmins with intermediate experience_"]

### What are the requirements?

[Software, hardware, and/or OS required to follow your article.]

### What's your ETA?

[An estimation of the time you'll need to submit your draft for evaluation]

----

## Proposal

### Writing sample

[Link to any technical material previously written by the yourself, preferably on the same or similar topic.]

### Introduction

[Write the introduction of your article (100-150 words). We'll evaluate your approach, writing style, organization, clearness, and conciseness.]

### Outlines

[Your article's topic structure. We'll evaluate how you structure your subject in sub-topics, the depth of your content and the logic of your construction. Your outlines should respond: what is required to get started, what problem does your article solve, why is it important, what's the motivation to follow your method.]

### Example project

[If your method needs a GitLab project, please create one, make it work as you want, make it publicly available, and link it here. If you don't have a working project yet, please do it before submitting your proposal.]

----

## Warnings

- Before creating a new issue, please search for existing ones to avoid creating duplicates.
- The **issue title** is the title of the article you're proposing to write about.
- Please visit the webpage for an overview of the [Community Writers Program](https://about.gitlab.com/community-writers/).
- Please advise, for this program you'll write blog posts for the [GitLab blog](https://about.gitlab.com/blog) according to the [editorial style guide](https://gitlab.com/gitlab-com/marketing/blob/master/content/editorial-style-guide.md).
- Once all the fields of this template are completed, please add **a comment** to the thread:

    ```md
    @rebecca I would like to write about this subject and I accept the
    [terms and conditions](https://about.gitlab.com/community-writers/terms-and-conditions/)
    of the Community Writers Program.
    ```
