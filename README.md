## GitLab Community Writers Program

For an overview of the program, please visit the [webpage](https://about.gitlab.com/community-writers/).

This project is the issue tracker for proposals of blog posts for the [GitLab blog](https://about.gitlab.com/blog)
to be written by community writers.

All issues labeled with "up-for-grabs" can be taken by the community. If you have a new proposal,
you're welcome to create a new issue.

Any proposal sent by prospect community writers are subjected to the
[terms and condictions](https://about.gitlab.com/community-writers/terms-and-conditions/).

## License

Please check the [contribution guide](https://gitlab.com/gitlab-com/community-writers/blob/master/CONTRIBUTING.md).
